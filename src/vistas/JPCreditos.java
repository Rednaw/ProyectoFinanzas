package vistas;

import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class JPCreditos extends javax.swing.JPanel {

    public static String fecha = "1980-10-12";
    public static String fecha2 = "2030-10-12";

    public JPCreditos() {
        initComponents();
        addTableValueIngresos();
    }

    public static void addTableValueIngresos() {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet contadorIngresosDelUser = mysql.consultar("Select* from creditos inner join "
                + "usuarios_cuentas ON usuarios_cuentas.usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id=  creditos.cuenta_id && creditos.fechacreacion"
                + " BETWEEN '" + fecha + "' AND '" + fecha2 + "';");
        ResultSet resultados = mysql.consultar("Select* from creditos inner join "
                + "usuarios_cuentas ON usuarios_cuentas.usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id=  creditos.cuenta_id && creditos.fechacreacion"
                + " BETWEEN '" + fecha + "' AND '" + fecha2 + "';");
        String[][] valores;
        int contador = 0;
        if (contadorIngresosDelUser != null) {
            try {
                while (contadorIngresosDelUser.next()) {
                    contador++;
                }
            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);
            }
            int fila = 0;
            valores = new String[contador][5];
            if (resultados != null) {
                try {
                    while (resultados.next()) {
                        valores[fila][0] = resultados.getString("Descripcion");
                        valores[fila][1] = resultados.getString("TipoPeriodo_id");
                        valores[fila][2] = resultados.getString("FechaCreacion");
                        valores[fila][3] = resultados.getString("TipoTAE");
                        valores[fila][4] = resultados.getString("Saldo");
                        fila++;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(JFrameAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            jTableCreditos.setModel(new javax.swing.table.DefaultTableModel(
                    valores,
                    new String[]{
                        "Descripción", "Tipo Periodo", "Fecha Creacion", "%TAE", "Saldo"
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCreditos = new javax.swing.JTable();
        jButtonActualizar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxDia4 = new javax.swing.JComboBox<>();
        jComboBoxMes4 = new javax.swing.JComboBox<>();
        jComboBoxAnyo4 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxDia5 = new javax.swing.JComboBox<>();
        jComboBoxMes5 = new javax.swing.JComboBox<>();
        jComboBoxAnyo5 = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(649, 400));
        setLayout(null);

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/Baners blancos/Consuta tus creditos.png"))); // NOI18N
        add(jLabel2);
        jLabel2.setBounds(-310, 0, 810, 65);

        jButton1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton1.setText("Agregar crédito");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(520, 160, 120, 40);

        addTableValueIngresos();
        jTableCreditos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableCreditos);

        add(jScrollPane1);
        jScrollPane1.setBounds(20, 160, 490, 230);

        jButtonActualizar.setText("Actualizar");
        jButtonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActualizarActionPerformed(evt);
            }
        });
        add(jButtonActualizar);
        jButtonActualizar.setBounds(380, 90, 110, 40);

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel3.setText("Desde");
        add(jLabel3);
        jLabel3.setBounds(40, 80, 70, 30);

        jComboBoxDia4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DIA", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDia4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDia4ActionPerformed(evt);
            }
        });
        add(jComboBoxDia4);
        jComboBoxDia4.setBounds(100, 80, 70, 26);

        jComboBoxMes4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MES", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        add(jComboBoxMes4);
        jComboBoxMes4.setBounds(180, 80, 70, 26);

        jComboBoxAnyo4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AÑO", "1995", "1996", "1996", "1997", "1997", "1998", "1998", "1999", "1999", "2000", "2000", "2001", "2001", "2002", "2002", "2003", "2003", "2004", "2004", "2005", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" }));
        jComboBoxAnyo4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnyo4ActionPerformed(evt);
            }
        });
        add(jComboBoxAnyo4);
        jComboBoxAnyo4.setBounds(260, 80, 90, 26);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setText("Hasta");
        add(jLabel4);
        jLabel4.setBounds(40, 120, 50, 30);

        jComboBoxDia5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DIA", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDia5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDia5ActionPerformed(evt);
            }
        });
        add(jComboBoxDia5);
        jComboBoxDia5.setBounds(100, 120, 70, 26);

        jComboBoxMes5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MES", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        add(jComboBoxMes5);
        jComboBoxMes5.setBounds(180, 120, 70, 26);

        jComboBoxAnyo5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AÑO", "1995", "1996", "1996", "1997", "1997", "1998", "1998", "1999", "1999", "2000", "2000", "2001", "2001", "2002", "2002", "2003", "2003", "2004", "2004", "2005", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" }));
        jComboBoxAnyo5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnyo5ActionPerformed(evt);
            }
        });
        add(jComboBoxAnyo5);
        jComboBoxAnyo5.setBounds(260, 120, 90, 26);

        jButton2.setText("Eliminar");
        add(jButton2);
        jButton2.setBounds(520, 348, 120, 40);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        add(jLabel5);
        jLabel5.setBounds(-190, -100, 850, 530);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFrameAgregarCredito credito = new JFrameAgregarCredito();
        credito.setLocationRelativeTo(null);
        credito.setVisible(true);

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBoxDia4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDia4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxDia4ActionPerformed

    private void jComboBoxAnyo4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnyo4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAnyo4ActionPerformed

    private void jComboBoxDia5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDia5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxDia5ActionPerformed

    private void jComboBoxAnyo5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnyo5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAnyo5ActionPerformed

    private void jButtonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActualizarActionPerformed
      setFecha();
      addTableValueIngresos();
    }//GEN-LAST:event_jButtonActualizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButtonActualizar;
    public javax.swing.JComboBox<String> jComboBoxAnyo4;
    public javax.swing.JComboBox<String> jComboBoxAnyo5;
    public javax.swing.JComboBox<String> jComboBoxDia4;
    public javax.swing.JComboBox<String> jComboBoxDia5;
    public javax.swing.JComboBox<String> jComboBoxMes4;
    public javax.swing.JComboBox<String> jComboBoxMes5;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTableCreditos;
    // End of variables declaration//GEN-END:variables

    public void setFecha(){
        if (jComboBoxDia4.getSelectedItem().equals("DIA") || jComboBoxMes4.
                getSelectedItem().equals("MES") || jComboBoxAnyo4.getSelectedItem().equals("AÑO")
                || jComboBoxDia5.getSelectedItem().equals("DIA") || jComboBoxMes5.getSelectedItem().equals("MES")
                || jComboBoxAnyo5.getSelectedItem().equals("AÑO")) {
            JOptionPane.showMessageDialog(jButton1, "Inserte una fecha valida");
        } else {
            fecha = jComboBoxAnyo4.getSelectedItem().toString() + "-"
                    + jComboBoxMes4.getSelectedItem().toString() + "-"
                    + jComboBoxDia4.getSelectedItem().toString();
            fecha2 = jComboBoxAnyo5.getSelectedItem().toString() + "-"
                    + jComboBoxMes5.getSelectedItem().toString() + "-"
                    + jComboBoxDia5.getSelectedItem().toString();
        }
    }
    
    
    
    
}
