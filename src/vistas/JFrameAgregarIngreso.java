package vistas;

import controladores.ControladorAgregarIngreso;
import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static vistas.JFrameAgregarTarjeta.cuentaJF_id;
import static vistas.JFrameAgregarTarjeta.jComboBoxCuentaAsociada;
import static vistas.JFrameAgregarTarjeta.jComboBoxCuentaAsociada;

public class JFrameAgregarIngreso extends javax.swing.JFrame {

    public static int[] cuentaJF_id;

    public JFrameAgregarIngreso() {
        initComponents();
        ControladorAgregarIngreso c = new ControladorAgregarIngreso(this);
    }

    public void jComboBoxCuentaAsociada() {
        MySQLBD mysqlq = new MySQLBD().conectar();
        ResultSet contador = mysqlq.consultar("SELECT cuentas.NumeroIBAN from cuentas inner join usuarios_cuentas"
                + " on usuarios_cuentas.usuario_id=" + ControladorIniciarSesion.ID + " "
                + "&& cuentas.cuenta_id=usuarios_cuentas.cuenta_id; ");
        ResultSet resultadoIBAN = mysqlq.consultar("SELECT cuentas.NumeroIBAN from cuentas inner join usuarios_cuentas"
                + " on usuarios_cuentas.usuario_id=" + ControladorIniciarSesion.ID + " "
                + "&& cuentas.cuenta_id=usuarios_cuentas.cuenta_id; ");
        ResultSet listaID = mysqlq.consultar("SELECT cuentas.cuenta_id from cuentas inner join usuarios_cuentas on usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id = cuentas.cuenta_id;");
        int fila = 0;
        int contador2 = 0;
        jComboBoxCuentaAsociada.addItem("Seleccione una cuenta");
        fila++;
        contador2++;
        try {
            while (contador.next()) {
                contador2++;

            }
        } catch (SQLException ex) {
            Logger.getLogger(JFrameAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
        }
        cuentaJF_id = new int[contador2];

        try {
            while (resultadoIBAN.next() && listaID.next()) {
                jComboBoxCuentaAsociada.addItem(resultadoIBAN.getString("NumeroIBAN"));
                cuentaJF_id[fila] = listaID.getInt("Cuenta_id");
                System.out.println("cuenta id: " + cuentaJF_id[fila]);
                fila++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(JFrameAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void jComboBoxTipoPeriodo() {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoNombre = mysql.consultar("select Descripcion from tiposperiodos");
        jComboBoxTipoPeriodo.addItem("Seleccione el periodo");
        if (resultadoNombre != null) {
            try {
                System.out.println("LlegamosTRY");
                while (resultadoNombre.next()) {
                    jComboBoxTipoPeriodo.addItem(resultadoNombre.getString("Descripcion"));
                }

            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldDescripcion = new javax.swing.JTextField();
        jTextFieldEmisor = new javax.swing.JTextField();
        jComboBoxCuentaAsociada = new javax.swing.JComboBox<>();
        jComboBoxTipoPeriodo = new javax.swing.JComboBox<>();
        jTextFieldValor = new javax.swing.JTextField();
        jButtonAgregarIngreso = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jComboBoxDia = new javax.swing.JComboBox<>();
        jComboBoxMes = new javax.swing.JComboBox<>();
        jComboBoxAnyo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 500));
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/agregar un ingreso.PNG"))); // NOI18N
        jLabel2.setToolTipText("");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(100, 10, 430, 80);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setText("Descripción");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(70, 100, 130, 40);

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setText("Emisor");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(70, 160, 70, 30);

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel6.setText("Periodo");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(340, 260, 70, 40);

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel7.setText("Cuenta relacionada");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(70, 260, 150, 40);

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel8.setText("Fecha en que se realizó");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(70, 210, 140, 30);

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel9.setText("Valor del ingreso");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(500, 260, 140, 40);
        getContentPane().add(jTextFieldDescripcion);
        jTextFieldDescripcion.setBounds(220, 100, 260, 30);
        getContentPane().add(jTextFieldEmisor);
        jTextFieldEmisor.setBounds(220, 160, 170, 30);

        jComboBoxCuentaAsociada();
        getContentPane().add(jComboBoxCuentaAsociada);
        jComboBoxCuentaAsociada.setBounds(10, 300, 280, 30);

        jComboBoxTipoPeriodo();
        getContentPane().add(jComboBoxTipoPeriodo);
        jComboBoxTipoPeriodo.setBounds(320, 300, 130, 30);
        getContentPane().add(jTextFieldValor);
        jTextFieldValor.setBounds(500, 300, 100, 30);

        jButtonAgregarIngreso.setText("Agregar");
        jButtonAgregarIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarIngresoActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAgregarIngreso);
        jButtonAgregarIngreso.setBounds(120, 390, 120, 40);

        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(380, 390, 120, 40);

        jComboBoxDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DIA", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDiaActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxDia);
        jComboBoxDia.setBounds(220, 220, 70, 26);

        jComboBoxMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MES", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        getContentPane().add(jComboBoxMes);
        jComboBoxMes.setBounds(300, 220, 70, 26);

        jComboBoxAnyo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AÑO", "1995", "1996", "1996", "1997", "1997", "1998", "1998", "1999", "1999", "2000", "2000", "2001", "2001", "2002", "2002", "2003", "2003", "2004", "2004", "2005", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" }));
        jComboBoxAnyo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnyoActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxAnyo);
        jComboBoxAnyo.setBounds(380, 220, 90, 26);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAgregarIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarIngresoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAgregarIngresoActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBoxDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxDiaActionPerformed

    private void jComboBoxAnyoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnyoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAnyoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarIngreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarIngreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarIngreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarIngreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameAgregarIngreso().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    public javax.swing.JButton jButtonAgregarIngreso;
    public javax.swing.JComboBox<String> jComboBoxAnyo;
    public javax.swing.JComboBox<String> jComboBoxCuentaAsociada;
    public javax.swing.JComboBox<String> jComboBoxDia;
    public javax.swing.JComboBox<String> jComboBoxMes;
    public javax.swing.JComboBox<String> jComboBoxTipoPeriodo;
    public javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JTextField jTextFieldDescripcion;
    public javax.swing.JTextField jTextFieldEmisor;
    public javax.swing.JTextField jTextFieldValor;
    // End of variables declaration//GEN-END:variables
}
