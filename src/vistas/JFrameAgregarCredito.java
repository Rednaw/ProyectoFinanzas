
package vistas;

import controladores.ControladorAgregarCredito;
import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JFrameAgregarCredito extends javax.swing.JFrame {
    public static  int[] cuentaJFCredito_id;
   
    public JFrameAgregarCredito() {
        initComponents();
        ControladorAgregarCredito c = new ControladorAgregarCredito(this);
    }
    public void jComboBoxCuentaAsociada(){
        MySQLBD mysqlq = new MySQLBD().conectar();
        ResultSet contadorCuentas= mysqlq.consultar("SELECT cuentas.NumeroIBAN from cuentas inner join usuarios_cuentas"
                + " on usuarios_cuentas.usuario_id=" + ControladorIniciarSesion.ID 
                + "&& cuentas.cuenta_id=usuarios_cuentas.cuenta_id; ");
        ResultSet resultadoIBAN = mysqlq.consultar("SELECT cuentas.NumeroIBAN from cuentas inner join usuarios_cuentas"
                + " on usuarios_cuentas.usuario_id=" + ControladorIniciarSesion.ID 
                + "&& cuentas.cuenta_id=usuarios_cuentas.cuenta_id; ");
        ResultSet listaID = mysqlq.consultar("SELECT cuentas.cuenta_id from cuentas inner join usuarios_cuentas on usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id = cuentas.cuenta_id;");
        int fila = 0;
        int contador = 0;
       jComboBoxCuentaAsociada.addItem("Seleccione una cuenta");
        fila++;
        contador++;
        try {
            while(contadorCuentas.next()){
               contador++; 
            }
        } catch (SQLException ex) {
            Logger.getLogger(JFrameAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);
        }
        cuentaJFCredito_id = new int [contador];
        try {
            while(resultadoIBAN.next() && listaID.next()){
                jComboBoxCuentaAsociada.addItem(resultadoIBAN.getString("NumeroIBAN"));
                cuentaJFCredito_id[fila]= listaID.getInt("Cuenta_id");
                System.out.println("Cuenta ID Prestamo: "+cuentaJFCredito_id[fila]);
                fila++;
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(JFrameAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void jComboBoxTipoPeriodo() {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoNombre = mysql.consultar("select Descripcion from tiposperiodos");
        jComboBoxPeriodo.addItem("Seleccione el periodo");
        if (resultadoNombre != null) {
            try {
                System.out.println("LlegamosTRY");
                while (resultadoNombre.next()) {
                    jComboBoxPeriodo.addItem(resultadoNombre.getString("Descripcion"));
                }

            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldDescripcion = new javax.swing.JTextField();
        jComboBoxCuentaAsociada = new javax.swing.JComboBox<>();
        jComboBoxPeriodo = new javax.swing.JComboBox<>();
        jTextFieldTAE = new javax.swing.JTextField();
        jTextFieldLimite = new javax.swing.JTextField();
        jTextFieldSaldo = new javax.swing.JTextField();
        jButtonAgregarCredito = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jComboBoxDia = new javax.swing.JComboBox<>();
        jComboBoxMes = new javax.swing.JComboBox<>();
        jComboBoxAnyo = new javax.swing.JComboBox<>();
        jComboBoxDia1 = new javax.swing.JComboBox<>();
        jComboBoxMes1 = new javax.swing.JComboBox<>();
        jComboBoxAnyo1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agregar un crédito");
        setMinimumSize(new java.awt.Dimension(640, 480));
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/agregar un credito.PNG"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(110, 0, 420, 80);

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel3.setText("Descripción");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(90, 100, 120, 40);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setText("Cuenta relacionada");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(50, 150, 114, 30);

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setText("Periodo");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(100, 190, 60, 30);

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel6.setText("Saldo");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(440, 310, 50, 30);

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel7.setText("Interes TAE");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(110, 310, 80, 30);

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel8.setText("Fecha de inicio");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(100, 230, 100, 30);

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel9.setText("Limite");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(290, 310, 50, 30);

        jLabel10.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel10.setText("Fecha de vencimiento");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(380, 230, 140, 30);
        getContentPane().add(jTextFieldDescripcion);
        jTextFieldDescripcion.setBounds(190, 100, 330, 30);

        jComboBoxCuentaAsociada();
        getContentPane().add(jComboBoxCuentaAsociada);
        jComboBoxCuentaAsociada.setBounds(190, 150, 270, 30);

        jComboBoxTipoPeriodo();
        getContentPane().add(jComboBoxPeriodo);
        jComboBoxPeriodo.setBounds(190, 190, 320, 30);
        getContentPane().add(jTextFieldTAE);
        jTextFieldTAE.setBounds(110, 340, 80, 30);
        getContentPane().add(jTextFieldLimite);
        jTextFieldLimite.setBounds(280, 340, 80, 30);
        getContentPane().add(jTextFieldSaldo);
        jTextFieldSaldo.setBounds(440, 340, 80, 30);

        jButtonAgregarCredito.setText("Agregar");
        jButtonAgregarCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarCreditoActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAgregarCredito);
        jButtonAgregarCredito.setBounds(130, 390, 120, 40);

        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(390, 390, 120, 40);

        jComboBoxDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DIA", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDiaActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxDia);
        jComboBoxDia.setBounds(60, 270, 70, 20);

        jComboBoxMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MES", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        getContentPane().add(jComboBoxMes);
        jComboBoxMes.setBounds(140, 270, 70, 20);

        jComboBoxAnyo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AÑO", "1995", "1996", "1996", "1997", "1997", "1998", "1998", "1999", "1999", "2000", "2000", "2001", "2001", "2002", "2002", "2003", "2003", "2004", "2004", "2005", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" }));
        jComboBoxAnyo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnyoActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxAnyo);
        jComboBoxAnyo.setBounds(220, 270, 90, 20);

        jComboBoxDia1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DIA", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDia1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDia1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxDia1);
        jComboBoxDia1.setBounds(350, 270, 70, 20);

        jComboBoxMes1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MES", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        getContentPane().add(jComboBoxMes1);
        jComboBoxMes1.setBounds(430, 270, 70, 20);

        jComboBoxAnyo1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AÑO", "1995", "1996", "1996", "1997", "1997", "1998", "1998", "1999", "1999", "2000", "2000", "2001", "2001", "2002", "2002", "2003", "2003", "2004", "2004", "2005", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" }));
        jComboBoxAnyo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAnyo1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxAnyo1);
        jComboBoxAnyo1.setBounds(510, 270, 90, 20);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
this.dispose();

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButtonAgregarCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarCreditoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAgregarCreditoActionPerformed

    private void jComboBoxDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxDiaActionPerformed

    private void jComboBoxAnyoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnyoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAnyoActionPerformed

    private void jComboBoxDia1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDia1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxDia1ActionPerformed

    private void jComboBoxAnyo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAnyo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAnyo1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarCredito.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarCredito.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarCredito.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameAgregarCredito.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameAgregarCredito().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButtonAgregarCredito;
    public javax.swing.JComboBox<String> jComboBoxAnyo;
    public javax.swing.JComboBox<String> jComboBoxAnyo1;
    public javax.swing.JComboBox<String> jComboBoxCuentaAsociada;
    public javax.swing.JComboBox<String> jComboBoxDia;
    public javax.swing.JComboBox<String> jComboBoxDia1;
    public javax.swing.JComboBox<String> jComboBoxMes;
    public javax.swing.JComboBox<String> jComboBoxMes1;
    public javax.swing.JComboBox<String> jComboBoxPeriodo;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JTextField jTextFieldDescripcion;
    public javax.swing.JTextField jTextFieldLimite;
    public javax.swing.JTextField jTextFieldSaldo;
    public javax.swing.JTextField jTextFieldTAE;
    // End of variables declaration//GEN-END:variables


}
