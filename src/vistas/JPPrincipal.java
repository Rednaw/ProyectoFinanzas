/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JPPrincipal extends javax.swing.JPanel {

    //private JTextField tf1;
    //private JTextField tf2;
    //private JTextField tf3;
    private boolean bandera = false;

    public JPPrincipal() {
        
        initComponents();
    }
    
    public void paint(Graphics g) {
        super.paint(g);

        if (bandera == true) {
            MySQLBD mysql = new MySQLBD().conectar();
            ResultSet resultadoV1 = mysql.consultar("SELECT valor FROM conceptosgastos INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="
                    + ControladorIniciarSesion.ID + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && TipoCategoriaGasto_id=1");
            double v1 = 0;
            if (resultadoV1 != null) {
                try {
                    while (resultadoV1.next()) {
                        v1 = v1 + resultadoV1.getDouble("valor");
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(JPPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            double v2 = 0;
            ResultSet resultadoV2 = mysql.consultar(" SELECT valor FROM conceptosgastos INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="
                    + ControladorIniciarSesion.ID + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && TipoCategoriaGasto_id=2");

            if (resultadoV2 != null) {
                try {
                    while (resultadoV2.next()) {
                        v2 = v2 + resultadoV2.getDouble("valor");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(JPPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            double v3 = 0;
            ResultSet resultadoV3 = mysql.consultar(" SELECT valor FROM conceptosgastos INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="
                    + ControladorIniciarSesion.ID + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && TipoCategoriaGasto_id=3");

            if (resultadoV3 != null) {
                try {
                    while (resultadoV3.next()) {
                        v3 = v3 + resultadoV3.getDouble("valor");
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(JPPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            double v4 = 0;
            ResultSet resultadoV4 = mysql.consultar(" SELECT valor FROM conceptosgastos INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="
                    + ControladorIniciarSesion.ID + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && TipoCategoriaGasto_id=4");

            if (resultadoV4 != null) {
                try {
                    while (resultadoV4.next()) {
                        v4 = v4 + resultadoV4.getDouble("valor");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(JPPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            double v5 = 0;
            ResultSet resultadoV5 = mysql.consultar(" SELECT valor FROM conceptosgastos INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="
                    + ControladorIniciarSesion.ID + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && TipoCategoriaGasto_id=5");

            if (resultadoV5 != null) {
                try {
                    while (resultadoV5.next()) {
                        v5 = v5 + resultadoV5.getDouble("valor");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(JPPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            double mayor = retornarMayor(v1, v2, v3, v4, v5);

            double largo1 = v1 * 280 / mayor;
            double largo2 = v2 * 280 / mayor;
            double largo3 = v3 * 280 / mayor;
            double largo4 = v4 * 280 / mayor;
            double largo5 = v5 * 280 / mayor;

            g.setColor(new Color(171, 0, 114));
            g.setFont(new java.awt.Font("Calibri Light", 1, 15));
            g.fillRect(120, 230, (int) largo1, 20);
            g.drawString("Suministros", 30, 245);
            

            g.setColor(new Color(0, 125, 190));
            g.setFont(new java.awt.Font("Calibri Light", 1, 15));
            g.fillRect(120 , 260, (int) largo2, 20);
            g.drawString("Servicios", 30, 275);

            g.setColor(new Color(0, 0, 255));
            g.setFont(new java.awt.Font("Calibri Light", 1, 15));
            g.fillRect(120, 290, (int) largo3, 20);
            g.drawString("Seguros", 30, 305);

            g.setColor(new Color(78, 160, 110));
            g.setFont(new java.awt.Font("Calibri Light", 1, 15));
            g.fillRect(120, 320, (int) largo4, 20);
            g.drawString("Hacienda", 30, 335);

            g.setColor(new Color(0, 44, 107));
            g.setFont(new java.awt.Font("Calibri Light", 1, 15));
            g.fillRect(120, 350, (int) largo5, 20);
            g.drawString("Facturas", 30, 365);

        }
    }

    private double retornarMayor(double v1, double v2, double v3, double v4, double v5) {
        if (v1 > v2 && v1 > v3 && v1 > v4 && v1 > v5) {
            return v1;
        } else if (v2 > v3 && v2 > v4 && v2 > v5) {
            return v2;
        } else if (v3 > v4 && v3 > v5) {
            return v3;
        } else if (v4 > v5) {
            return v4;
        } else {
            return v5;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelVentana = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        contentPane = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Relevante2 = new javax.swing.JLabel();
        jLabelRelevante1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setLayout(null);

        jPanelVentana.setBackground(new java.awt.Color(255, 255, 255));
        jPanelVentana.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Calibri Light", 1, 24)); // NOI18N
        jLabel1.setText("Agregar");
        jPanelVentana.add(jLabel1);
        jLabel1.setBounds(530, 159, 82, 30);

        jButton1.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jButton1.setText("Gasto");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanelVentana.add(jButton1);
        jButton1.setBounds(508, 195, 119, 35);

        jButton2.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jButton2.setText("Ingreso");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanelVentana.add(jButton2);
        jButton2.setBounds(508, 242, 119, 35);

        jButton3.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jButton3.setText("Prestamo");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanelVentana.add(jButton3);
        jButton3.setBounds(508, 295, 119, 35);

        bandera = true;
        repaint();
        contentPane.setLayout(null);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        contentPane.add(jLabel4);
        jLabel4.setBounds(-200, -290, 850, 530);

        jPanelVentana.add(contentPane);
        contentPane.setBounds(10, 196, 480, 190);

        jButton4.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jButton4.setText("Crédito");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanelVentana.add(jButton4);
        jButton4.setBounds(508, 342, 119, 35);

        jLabel2.setFont(new java.awt.Font("Calibri Light", 1, 36)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/Baners blancos/Balande de gastos.png"))); // NOI18N
        jLabel2.setText("Balance de gastos");
        jPanelVentana.add(jLabel2);
        jLabel2.setBounds(-320, 0, 840, 65);

        jLabel5.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        jLabel5.setText("Gastos relevantes del mes");
        jPanelVentana.add(jLabel5);
        jLabel5.setBounds(30, 70, 230, 30);

        Relevante2.setText("Seguro 250€");
        jPanelVentana.add(Relevante2);
        Relevante2.setBounds(30, 120, 170, 50);

        jLabelRelevante1.setText("Endesa 110€");
        jPanelVentana.add(jLabelRelevante1);
        jLabelRelevante1.setBounds(30, 160, 140, 20);

        jLabel6.setText("VISA 300€");
        jPanelVentana.add(jLabel6);
        jLabel6.setBounds(30, 110, 90, 16);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        jPanelVentana.add(jLabel3);
        jLabel3.setBounds(-190, -100, 850, 530);

        add(jPanelVentana);
        jPanelVentana.setBounds(0, 0, 659, 435);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFrameAgregarGasto gasto = new JFrameAgregarGasto();
        gasto.setLocationRelativeTo(null);
        gasto.setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        JFrameAgregarIngreso ingreso = new JFrameAgregarIngreso();
        ingreso.setVisible(true);
        ingreso.setLocationRelativeTo(null);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JFrameAgregarPrestamo pre = new JFrameAgregarPrestamo();
        pre.setLocationRelativeTo(null);
        pre.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        JFrameAgregarCredito credito = new JFrameAgregarCredito();
        credito.setVisible(true);
        credito.setLocationRelativeTo(null);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Relevante2;
    private javax.swing.JPanel contentPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelRelevante1;
    private javax.swing.JPanel jPanelVentana;
    // End of variables declaration//GEN-END:variables
}
    /*
    public void paint(Graphics g) {
        super.paint(g);
        if (bandera == true) {

            double v1 = 2;
            double v2 = 3;
            double v3 = 3;
            double v4 = 2;
            double v5 = 2;
            double suma = v1 + v2 + v3 + v4 + v5;
            double grados1 = v1 * 360 / suma;
            double grados2 = v2 * 360 / suma;
            double grados3 = v3 * 360 / suma;
            double grados4 = v4 * 360 / suma;
            double grados5 = v5 * 360 / suma;

            g.setColor(new Color(255, 0, 0));
            g.fillArc(50, 25, 200, 200, 0, (int) grados1);
            g.fillRect(50, 250, 20, 20);
            g.drawString("Suministros", 100, 270);

            g.setColor(new Color(0, 128, 0));
            g.fillArc(50, 25, 200, 200, (int) grados1, (int) grados2);
            g.fillRect(50, 280, 20, 20);
            g.drawString("Servicios", 100, 300);

            g.setColor(new Color(0, 0, 255));
            g.fillArc(50, 25, 200, 200, (int) grados1 + (int) grados2, (int) grados3);
            g.fillRect(50, 310, 20, 20);
            g.drawString("Seguros", 100, 330);

            g.setColor(new Color(0, 0, 200));
            g.fillArc(50, 25, 200, 200, (int) grados1 + (int) grados2 + (int) grados3, (int) grados4);
            g.fillRect(50, 340, 20, 20);
            g.drawString("Facturas", 100, 360);

            g.setColor(new Color(0, 0, 110));
            g.fillArc(50, 25, 200, 200, (int) grados1 + (int) grados2 + (int) grados3 + (int) grados4, (int) grados5);
            g.fillRect(50, 370, 20, 20);
            g.drawString("Prestamos", 100, 390);
        }
    }
     
 
    public GraficoBarraHorizontal() {
        
        setBounds(100, 100, 800, 600);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblPartido = new JLabel("Partido 1:");
        lblPartido.setBounds(46, 39, 61, 14);
        contentPane.add(lblPartido);

        JLabel lblPartido_1 = new JLabel("Partido 2:");
        lblPartido_1.setBounds(46, 69, 61, 14);
        contentPane.add(lblPartido_1);

        JLabel lblPartido_2 = new JLabel("Partido 3:");
        lblPartido_2.setBounds(46, 103, 61, 14);
        contentPane.add(lblPartido_2);

        tf1 = new JTextField();
        tf1.setBounds(117, 36, 86, 20);
        contentPane.add(tf1);
        tf1.setColumns(10);

        tf2 = new JTextField();
        tf2.setBounds(117, 66, 86, 20);
        contentPane.add(tf2);
        tf2.setColumns(10);

        tf3 = new JTextField();
        tf3.setBounds(117, 97, 86, 20);
        contentPane.add(tf3);
        tf3.setColumns(10);
        }
     */
