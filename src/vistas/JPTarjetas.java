/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class JPTarjetas extends javax.swing.JPanel {
    public static String fecha = "1980-10-12";
    public static String fecha2 = "2100-10-12";
    public JPTarjetas() {
        initComponents();
        addTableValueTarjetas();
    }
    
    public static void addTableValueTarjetas(){
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet contadorIngresosDelUser = mysql.consultar("Select* from tarjetas inner join "
                + "usuarios_cuentas ON usuarios_cuentas.usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id=  tarjetas.cuenta_id && tarjetas.fechacreacion"
                + " BETWEEN '" + fecha + "' AND '" + fecha2 + "';");
        ResultSet resultados = mysql.consultar("Select* from tarjetas inner join "
                + "usuarios_cuentas ON usuarios_cuentas.usuario_id= " + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id=  tarjetas.cuenta_id && tarjetas.fechacreacion"
                + " BETWEEN '" + fecha + "' AND '" + fecha2 + "';");
        String[][] valores;
        int contador = 0;
        if (contadorIngresosDelUser != null) {
            try {
                while (contadorIngresosDelUser.next()) {
                    contador++;
                }
            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
            }
            int fila = 0;
            valores = new String[contador][5];
            if (resultados != null) {
                try {
                    while (resultados.next()) {
                        valores[fila][0] = resultados.getString("NumeroTarjeta");
                        valores[fila][1] = resultados.getString("TitularTarjeta");
                        valores[fila][2] = resultados.getString("Descripcion");
                        valores[fila][3] = resultados.getString("FechaCreacion");
                        valores[fila][4] = resultados.getString("Saldo");
                        fila++;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(JFrameAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            jTableTarjetas.setModel(new javax.swing.table.DefaultTableModel(
                    valores,
                    new String[]{
                        "Numero Tarjeta", "Titular", "Descripción", "Fecha Vencimiento", "Saldo"
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });
        }
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableTarjetas = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();

        jToolBar1.setRollover(true);

        setMinimumSize(new java.awt.Dimension(649, 400));
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(649, 400));
        setLayout(null);
        add(jLabel1);
        jLabel1.setBounds(145, 140, 0, 0);

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/Baners blancos/Consulta tus tarjetas.png"))); // NOI18N
        add(jLabel3);
        jLabel3.setBounds(-290, 10, 780, 60);

        jButton1.setText("Añadir una tarjeta");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(500, 20, 150, 40);

        jTableTarjetas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableTarjetas);

        add(jScrollPane1);
        jScrollPane1.setBounds(10, 80, 640, 310);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        add(jLabel6);
        jLabel6.setBounds(-190, -100, 850, 530);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFrameAgregarTarjeta trj = new JFrameAgregarTarjeta();
        trj.setLocationRelativeTo(null);
        trj.setVisible(true);
      
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTableTarjetas;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    private void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    
    
    
}
