/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

/**
 *
 * @author root
 */
public class JPTransferencias extends javax.swing.JPanel {

    /**
     * Creates new form JPTransferencias
     */
    public JPTransferencias() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jComboBox4 = new javax.swing.JComboBox<>();
        jComboBox5 = new javax.swing.JComboBox<>();
        jComboBox6 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(649, 400));
        setPreferredSize(new java.awt.Dimension(649, 400));
        setLayout(null);

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/FotosBaners/Baners blancos/Consulta tus transferencias.png"))); // NOI18N
        add(jLabel2);
        jLabel2.setBounds(-210, 0, 720, 60);

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setText("Selecciona las fechas que quieras consultar");
        add(jLabel1);
        jLabel1.setBounds(40, 60, 260, 40);

        jLabel3.setText("Desde");
        add(jLabel3);
        jLabel3.setBounds(40, 90, 50, 40);

        jLabel4.setText("Hasta");
        add(jLabel4);
        jLabel4.setBounds(40, 130, 50, 40);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Día" }));
        add(jComboBox1);
        jComboBox1.setBounds(100, 100, 60, 26);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Día" }));
        add(jComboBox2);
        jComboBox2.setBounds(100, 140, 60, 26);

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
        add(jComboBox3);
        jComboBox3.setBounds(180, 100, 70, 26);

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
        add(jComboBox4);
        jComboBox4.setBounds(180, 140, 70, 26);

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Año" }));
        add(jComboBox5);
        jComboBox5.setBounds(260, 100, 70, 26);

        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Año" }));
        add(jComboBox6);
        jComboBox6.setBounds(260, 140, 70, 26);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Fecha envio", "Destinatario", "Moneda local", "Moneda extranjerab", "Cuenta Bancaria"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1);
        jScrollPane1.setBounds(10, 180, 640, 220);

        jButton1.setText("Agregar transferencia");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(500, 10, 150, 40);

        jButton2.setText("Actualizar lista");
        add(jButton2);
        jButton2.setBounds(360, 110, 110, 40);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        add(jLabel6);
        jLabel6.setBounds(-190, -100, 850, 530);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
JFrameAgregarTransferencia trans= new JFrameAgregarTransferencia();
trans.setLocationRelativeTo(null);
trans.setVisible(true);

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    
}
