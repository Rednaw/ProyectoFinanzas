package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class TarjetaDAO extends DAOBase {

    private String numeroTarjeta;
    private String descripcion;
    private int cuenta_id;
    private int titular_id;
    private String fechaCreacion;
    private char vencimientoMesAnyo;
    private Double tipoTAE;
    private Double limite;
    private Double saldo;
    private int tarjeta_id;
    private String titularTarjeta;

    public String getTitularTarjeta() {
        return titularTarjeta;
    }

    public void setTitularTarjeta(String titularTarjeta) {
        this.titularTarjeta = titularTarjeta;
    }

    public int getTarjeta_id() {
        return tarjeta_id;
    }

    public void setTarjeta_id(int tarjeta_id) {
        this.tarjeta_id = tarjeta_id;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTitular_id() {
        return titular_id;
    }

    public void setTitular_id(int titular_id) {
        this.titular_id = titular_id;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public char getVencimientoMesAnyo() {
        return vencimientoMesAnyo;
    }

    public void setVencimientoMesAnyo(char vencimientoMesAnyo) {
        this.vencimientoMesAnyo = vencimientoMesAnyo;
    }

    public Double getTipoTAE() {
        return tipoTAE;
    }

    public void setTipoTAE(Double tipoTAE) {
        this.tipoTAE = tipoTAE;
    }

    public Double getLimite() {
        return limite;
    }

    public void setLimite(Double limite) {
        this.limite = limite;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public TarjetaDAO() {
        super("Tarjetas");
        //addFieldDefinition(new DAOField("Tarjeta_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("NumeroTarjeta", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("TitularTarjeta", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        //addFieldDefinition(new DAOField("Titular_id", java.sql.Types.INTEGER, false));
        //addFieldDefinition(new DAOField("VencimientoMesAnyo", java.sql.Types.CHAR));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("TipoTAE", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Limite", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Saldo", java.sql.Types.DOUBLE));
    }

}
