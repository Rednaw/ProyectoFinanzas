package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class ConceptoGastoDAO extends DAOBase {

    private int conceptoGasto_id;
    private String Descripcion;
    private double Valor;
    private String Emisor;
    private int tipoCategoriaGasto_id;
    private int cuenta_id;
    private int tipoPeriodo_id;
    private String fechaCreacion;
    private String fechaBaja;
    private byte esExacto;

    public int getTipoCategoriaGasto_id() {
        return tipoCategoriaGasto_id;
    }

    public void setTipoCategoriaGasto_id(int tipoCategoriaGasto_id) {
        this.tipoCategoriaGasto_id = tipoCategoriaGasto_id;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTipoPeriodo_id() {
        return tipoPeriodo_id;
    }

    public void setTipoPeriodo_id(int tipoPeriodo_id) {
        this.tipoPeriodo_id = tipoPeriodo_id;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public byte isEsExacto() {
        return esExacto;
    }

    public void setEsExacto(byte esExacto) {
        this.esExacto = esExacto;
    }

    public int getConceptoGasto_id() {
        return conceptoGasto_id;
    }

    public void setConceptoGasto_id(int conceptoGasto_id) {
        this.conceptoGasto_id = conceptoGasto_id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public String getEmisor() {
        return Emisor;
    }

    public double getValor() {
        return Valor;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setEmisor(String Emisor) {
        this.Emisor = Emisor;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    public ConceptoGastoDAO() {
        super("ConceptosGastos");
        /*
        private int tipoCategoriaGasto_id;
        private int cuenta_id;
        private int tipoPeriodo_id;
        private String fechaCreacion;
        private String fechaBaja;
        private boolean esExacto;
         */

        addFieldDefinition(new DAOField("TipoCategoriaGasto_id", java.sql.Types.INTEGER, 11));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("Emisor", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, 11));
        addFieldDefinition(new DAOField("TipoPeriodo_id", java.sql.Types.INTEGER, 11));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("FechaBaja", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("EsExacto", java.sql.Types.TINYINT));

    }

}
