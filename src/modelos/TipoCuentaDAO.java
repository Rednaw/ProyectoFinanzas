package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TipoCuentaDAO extends DAOBase {

    private String descripcion;
    private int tipoCuenta_id;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoCuentaDAO() {
        super("tiposcuentas");
        addFieldDefinition(new DAOField("TipoCuenta_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 100));

    }

}
