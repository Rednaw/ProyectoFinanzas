package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class OpinionDAO extends DAOBase {

    private int usuario_id;
    private int tema_id;
    private String opinion;
    private int opinion_id;

    public int getOpinion_id() {
        return opinion_id;
    }

    public void setOpinion_id(int opinion_id) {
        this.opinion_id = opinion_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getTema_id() {
        return tema_id;
    }

    public void setTema_id(int tema_id) {
        this.tema_id = tema_id;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public OpinionDAO() {
        super("Opiniones");
        addFieldDefinition(new DAOField("Opinion_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Usuario_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Tema_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Opinion", java.sql.Types.VARCHAR, 1000));

    }

}
