package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class Usuario_CuentaDAO extends DAOBase {

    private int usuario_cuenta;

    public int getUsuario_cuenta() {
        return usuario_cuenta;
    }

    public void setUsuario_cuenta(int usuario_cuenta) {
        this.usuario_cuenta = usuario_cuenta;
    }
    private int cuenta_id;
    private int usuario_id;

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public Usuario_CuentaDAO() {
        super("usuarios_cuentas");
        addFieldDefinition(new DAOField("Usuario_Cuenta_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Usuario_id", java.sql.Types.INTEGER, false));
    }
}
