package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TitularDAO extends DAOBase {

    private String nombre;
    private int titular_id;
    private String apellido1;
    private String apellido2;

    public int getTitular_id() {
        return titular_id;
    }

    public void setTitular_id(int titular_id) {
        this.titular_id = titular_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public TitularDAO() {
        super("titulares");
        addFieldDefinition(new DAOField("Titular_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Nombre", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Apellido1", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Apellido2", java.sql.Types.VARCHAR, 60));

    }
}
