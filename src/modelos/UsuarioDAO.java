package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class UsuarioDAO extends DAOBase {

    private String nombreUsuario;
    private String password;
    private int usuario_id;
    private String email;
    private int nivelUsuario_id;
    private String apellido;
    private String DNI;

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNivelUsuario_id() {
        return nivelUsuario_id;
    }

    public void setNivelUsuario_id(int nivelusuario_id) {
        this.nivelUsuario_id = nivelusuario_id;
    }

    public UsuarioDAO() {
        super("usuarios");
        addFieldDefinition(new DAOField("Usuario_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("NombreUsuario", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("Password", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("Email", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("NivelUsuario_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Apellido", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("DNI", java.sql.Types.VARCHAR, 60));

    }

}
