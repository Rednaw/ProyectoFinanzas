package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class Cuenta_TitularDAO extends DAOBase {

    private int cuenta_id;
    private int titular_id;
    private int cuenta_Titular_id;

    public int getCuenta_titular_id() {
        return cuenta_Titular_id;
    }

    public void setCuenta_titular_id(int cuenta_titular_id) {
        this.cuenta_Titular_id = cuenta_titular_id;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTitular_id() {
        return titular_id;
    }

    public void setTitular_id(int titular_id) {
        this.titular_id = titular_id;
    }

    public Cuenta_TitularDAO() {
        super("Cuentas");
        addFieldDefinition(new DAOField("Cuenta_Titular_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, 11));
        addFieldDefinition(new DAOField("Titular_id", java.sql.Types.INTEGER, 11));

    }
}
