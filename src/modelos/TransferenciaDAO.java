package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TransferenciaDAO extends DAOBase {

    private int transferencia_id;
    private double valor;
    private String monedaAConvertir;
    private String valorMonedaConvertida;
    private String nombreReceptor;
    private String apellidoReceptor;
    private String dniReceptor;
    private String cuentaBancariaReceptor;
    private boolean enviado;

    public int getTransferencia_id() {
        return transferencia_id;
    }

    public void setTransferencia_id(int transferencia_id) {
        this.transferencia_id = transferencia_id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getMonedaAConvertir() {
        return monedaAConvertir;
    }

    public void setMonedaAConvertir(String monedaAConvertir) {
        this.monedaAConvertir = monedaAConvertir;
    }

    public String getValorMonedaConvertida() {
        return valorMonedaConvertida;
    }

    public void setValorMonedaConvertida(String valorMonedaConvertida) {
        this.valorMonedaConvertida = valorMonedaConvertida;
    }

    public String getNombreReceptor() {
        return nombreReceptor;
    }

    public void setNombreReceptor(String nombreReceptor) {
        this.nombreReceptor = nombreReceptor;
    }

    public String getApellidoReceptor() {
        return apellidoReceptor;
    }

    public void setApellidoReceptor(String apellidoReceptor) {
        this.apellidoReceptor = apellidoReceptor;
    }

    public String getDniReceptor() {
        return dniReceptor;
    }

    public void setDniReceptor(String dniReceptor) {
        this.dniReceptor = dniReceptor;
    }

    public String getCuentaBancariaReceptor() {
        return cuentaBancariaReceptor;
    }

    public void setCuentaBancariaReceptor(String cuentaBancariaReceptor) {
        this.cuentaBancariaReceptor = cuentaBancariaReceptor;
    }

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    public TransferenciaDAO() {
        super("transferencias");
        addFieldDefinition(new DAOField("Transferencia_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("MonedaAConvertir", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("NombreReceptor", java.sql.Types.VARCHAR, 50));
        addFieldDefinition(new DAOField("DniReceptor", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("CuentaBancariaReceptor", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("Enviado", java.sql.Types.TINYINT));

    }
}
