package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.TarjetaDAO;
import vistas.JFrameAgregarTarjeta;
import static vistas.JFrameAgregarTarjeta.jComboBoxCuentaAsociada;
import static vistas.JPTarjetas.addTableValueTarjetas;

public class ControladorAgregarTarjeta implements ActionListener {

    JFrameAgregarTarjeta vistaAgregar;
    TarjetaDAO modeloTarjeta = new TarjetaDAO();
    public static int tarjeta_id;
    public static int cuentaSeleccionada_id;

    public ControladorAgregarTarjeta(JFrameAgregarTarjeta controladorAgregarTarjeta) {
        this.vistaAgregar = controladorAgregarTarjeta;
        this.vistaAgregar.jButtonAgregarTarjeta.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String vacio = "";
        String tipoTarjeta;
        String titularTarjeta = vistaAgregar.jTextFieldTitularTarjeta.getText();
        String numeroTarjeta = vistaAgregar.jTextFieldNumeroTarjeta.getText();
        String dia = vistaAgregar.jComboBoxDia.getSelectedItem().toString();
        String mes = vistaAgregar.jComboBoxMes.getSelectedItem().toString();
        String anyo = vistaAgregar.jComboBoxAnyo.getSelectedItem().toString();
        String fecha = anyo + "-" + mes + "-" + dia;
        String tipoInteres = vistaAgregar.jTextFieldTipoInteres.getText();
        String limite = vistaAgregar.jTextFieldLimite.getText();
        String saldo = vistaAgregar.jTextFieldSaldo.getText();

        if (titularTarjeta.equals(vacio) || numeroTarjeta.equals(vacio) || dia.equals("DIA")
                || mes.equals("MES") || anyo.equals("anyo")) {
            JOptionPane.showMessageDialog(vistaAgregar, "Formato invalido, introduzca bien los campos");
            vistaAgregar.dispose();
            JFrameAgregarTarjeta vista = new JFrameAgregarTarjeta();
            vista.setVisible(true);

        } else {
            modeloTarjeta.setTitularTarjeta(titularTarjeta);
            modeloTarjeta.setNumeroTarjeta(numeroTarjeta);
            modeloTarjeta.setFechaCreacion(fecha);
            double tipoInteresADouble = Double.parseDouble(tipoInteres);
            modeloTarjeta.setTipoTAE(tipoInteresADouble);
            double limiteADouble = Double.parseDouble(limite);
            modeloTarjeta.setLimite(limiteADouble);
            double saldoADouble = Double.parseDouble(saldo);
            modeloTarjeta.setSaldo(saldoADouble);

            if (vistaAgregar.jRadioButtonDebito.isEnabled()) {
                modeloTarjeta.setDescripcion("Debito");
                tipoTarjeta = "Debito";
            } else {
                modeloTarjeta.setDescripcion("Credito");
                tipoTarjeta = "Credito";
            }
            this.cuentaSeleccionada_id = JFrameAgregarTarjeta.cuentaJF_id[vistaAgregar.jComboBoxCuentaAsociada.getSelectedIndex()];
            modeloTarjeta.setCuenta_id(cuentaSeleccionada_id);
            System.out.println("La id de la cuentaSeleccionada para la tarjeta es : "+cuentaSeleccionada_id);
            System.out.println(tipoInteresADouble);
            System.out.println(limiteADouble);
            System.out.println(saldoADouble);
            try {
                modeloTarjeta.save();
                getIDTarjeta();
                addTableValueTarjetas();
                System.out.println("La id de la tarjeta es: " + getIDTarjeta());

            } catch (DBDriverNotFound ex) {
                Logger.getLogger(ControladorAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    public int getIDTarjeta() {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("Select tarjeta_id from tarjetas WHERE cuenta_id="
                + cuentaSeleccionada_id
                + "&& NumeroTarjeta= '"
                + vistaAgregar.jTextFieldNumeroTarjeta.getText() + "';");
        if (resultadoID != null) {
            try {
                resultadoID.next();
                this.tarjeta_id = resultadoID.getInt("Tarjeta_id");
            } catch (SQLException ex) {
                Logger.getLogger(ControladorAgregarTarjeta.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return tarjeta_id;
    }

}
