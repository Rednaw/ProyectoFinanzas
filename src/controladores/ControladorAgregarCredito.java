package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.CreditoDAO;
import vistas.JFrameAgregarCredito;
import static vistas.JPCreditos.addTableValueIngresos;

public class ControladorAgregarCredito implements ActionListener {

    JFrameAgregarCredito vistaAgregarCredito;
    CreditoDAO modeloCredito = new CreditoDAO();
    public static int cuentaSeleccionada_id;

    public ControladorAgregarCredito(JFrameAgregarCredito controladorAgregarCredito) {

        this.vistaAgregarCredito = controladorAgregarCredito;
        this.vistaAgregarCredito.jButtonAgregarCredito.addActionListener(this);

    }

    @Override

    public void actionPerformed(ActionEvent e) {
        String vacio = "";

        String descripcion = vistaAgregarCredito.jTextFieldDescripcion.getText();
        String dia = vistaAgregarCredito.jComboBoxDia.getSelectedItem().toString();
        String mes = vistaAgregarCredito.jComboBoxMes.getSelectedItem().toString();
        String anyo = vistaAgregarCredito.jComboBoxAnyo.getSelectedItem().toString();
        String fecha = anyo + "-" + mes + "-" + dia;
        String dia1 = vistaAgregarCredito.jComboBoxDia1.getSelectedItem().toString();
        String mes1 = vistaAgregarCredito.jComboBoxMes1.getSelectedItem().toString();
        String anyo1 = vistaAgregarCredito.jComboBoxAnyo1.getSelectedItem().toString();
        String fecha1 = anyo1 + "-" + mes1 + "-" + dia1;
        String TAE = vistaAgregarCredito.jTextFieldTAE.getText();
        String limite = vistaAgregarCredito.jTextFieldLimite.getText();
        String saldo = vistaAgregarCredito.jTextFieldLimite.getText();
        if ((vistaAgregarCredito.jComboBoxDia.getSelectedIndex() == 0)
                || (vistaAgregarCredito.jComboBoxMes.getSelectedIndex() == 0)
                || (vistaAgregarCredito.jComboBoxAnyo.getSelectedIndex() == 0)
                || (descripcion.equals(vacio)) || TAE.equals(vacio) || limite.equals(vacio)
                || saldo.equals(vacio)) {
            JOptionPane.showMessageDialog(vistaAgregarCredito, "Rellene todos los cammpos");
            vistaAgregarCredito.dispose();
            JFrameAgregarCredito agr = new JFrameAgregarCredito();
            agr.setLocationRelativeTo(null);
            agr.setVisible(true);
        } else {
            modeloCredito.setDescripcion(descripcion);
            modeloCredito.setFechaCreacion(fecha);
            modeloCredito.setFechaVencimiento(fecha1);
            double TAEADouble = Double.parseDouble(TAE);
            modeloCredito.setTipoTAE(TAEADouble);
            double limiteADouble = Double.parseDouble(limite);
            modeloCredito.setLimite(limiteADouble);
            double saldoADouble = Double.parseDouble(saldo);
            modeloCredito.setSaldo(saldoADouble);
            this.cuentaSeleccionada_id = JFrameAgregarCredito.cuentaJFCredito_id[vistaAgregarCredito.jComboBoxCuentaAsociada.getSelectedIndex()];
            modeloCredito.setCuenta_id(cuentaSeleccionada_id);

            switch ((String) vistaAgregarCredito.jComboBoxPeriodo.getSelectedItem()) {

                case "Seleccione el periodo":
                    JOptionPane.showMessageDialog(vistaAgregarCredito, "Agrege un tipo de periodicidad valido");
                    vistaAgregarCredito.dispose();
                    JFrameAgregarCredito vistaAgregarCreditos = new JFrameAgregarCredito();
                    vistaAgregarCreditos.setLocationRelativeTo(null);
                    vistaAgregarCreditos.setVisible(true);
                    break;

                case "Mensual":
                    modeloCredito.setTipoPeriodo_id(1);
                    break;
                case "Anual":
                    modeloCredito.setTipoPeriodo_id(2);
                    break;
                case "Ocasional/Manual":
                    modeloCredito.setTipoPeriodo_id(3);
                    break;

                case "CREDITO":
                    modeloCredito.setTipoPeriodo_id(4);
                    break;
                case "Bimestral":
                    modeloCredito.setTipoPeriodo_id(5);
                    break;

                default:
                    modeloCredito.setTipoPeriodo_id(6);
                    break;

            }
        }

        try {
            modeloCredito.save();
            vistaAgregarCredito.dispose();
            addTableValueIngresos();
            System.out.println("La id del credito es: " + cuentaSeleccionada_id);

        } catch (DBDriverNotFound ex) {
            Logger.getLogger(ControladorAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex) {
            Logger.getLogger(ControladorAgregarCredito.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

}
