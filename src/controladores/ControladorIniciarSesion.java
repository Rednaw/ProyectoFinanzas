package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.String.valueOf;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vistas.JFFame;
import java.lang.Integer;
import vistas.JFrameIniciarSesion;
import static java.lang.String.valueOf;

public class ControladorIniciarSesion implements ActionListener {

    public String nombre;
    private String contrasenya;
    public static int ID = 0;

    JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();

    public ControladorIniciarSesion(JFrameIniciarSesion vistaIniciarSesion) {
        this.vistaIniciarSesion = vistaIniciarSesion;
        this.vistaIniciarSesion.jButtonAccederLog.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        nombre = vistaIniciarSesion.jTextFieldUsuarioLog.getText();
        this.nombre = nombre;
        char[] contrasenya = vistaIniciarSesion.jPasswordFieldContrasenyaLog.getPassword();
        this.contrasenya = valueOf(contrasenya);
        System.out.println("1pass:" + this.contrasenya);
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT NombreUsuario from Usuarios WHERE Password= '" + this.contrasenya
                + "' && NombreUsuario ='" + this.nombre + "';");
        try {
            if (resultadoID.next()) {
                if (this.nombre.equals(resultadoID.getString("nombreUsuario"))) {
                    JOptionPane.showMessageDialog(vistaIniciarSesion, "Bienvenido " + this.nombre);
                    try {
                        getID();
                    } catch (SQLException ex) {
                        Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    vistaIniciarSesion.dispose();
                    JFFame fame = new JFFame();
                    fame.setLocationRelativeTo(null);
                    fame.setVisible(true);
                } else {
                    vistaIniciarSesion.dispose();
                    JFrameIniciarSesion vista = new JFrameIniciarSesion();
                    vista.setLocationRelativeTo(null);
                    vista.setVisible(true);
                }

            } else {
                vistaIniciarSesion.dispose();
                JFrameIniciarSesion vista = new JFrameIniciarSesion();
                vista.setLocationRelativeTo(null);
                vista.setVisible(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getID() throws SQLException {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT Usuario_id from Usuarios WHERE NombreUsuario = '"
                + this.nombre + "' && Password = '" + this.contrasenya + "' ;");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("usuario_id"));
            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.ID = resultadoID.getInt("usuario_id");
        return ID;
    }

    /*public String getNombre(){
        MySQLBD mysql = new MySQLBD().conectar();
        mysql.consultar("SELECT NombreUsuario from Usuarios WHERE Password= '" + this.contrasenya
                + "' && NombreUsuario ='" + this.nombre + "';");
        return mysql.consultar(nombre);
        
    }*/
}
