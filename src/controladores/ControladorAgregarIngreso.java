package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.ConceptoIngresoDAO;
import vistas.JFrameAgregarIngreso;
import vistas.JPIngresos;
import static vistas.JPPrestamos.addTableValues;

public class ControladorAgregarIngreso implements ActionListener {

    JFrameAgregarIngreso vistaAgregar;
    ConceptoIngresoDAO modeloIngreso = new ConceptoIngresoDAO();
    public static int cuentaSeleccionada_id;

    public ControladorAgregarIngreso(JFrameAgregarIngreso controladorAgregarIngreso) {
        this.vistaAgregar = controladorAgregarIngreso;
        this.vistaAgregar.jButtonAgregarIngreso.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String vacio = "";
        String descripcion = vistaAgregar.jTextFieldDescripcion.getText();
        String emisor = vistaAgregar.jTextFieldEmisor.getText();
        String dia = vistaAgregar.jComboBoxDia.getSelectedItem().toString();
        String mes = vistaAgregar.jComboBoxMes.getSelectedItem().toString();
        String anyo = vistaAgregar.jComboBoxAnyo.getSelectedItem().toString();
        String fecha = anyo + "-" + mes + "-" + dia;
        String valor = vistaAgregar.jTextFieldValor.getText();
        if ((vistaAgregar.jComboBoxDia.getSelectedIndex() == 0)
                || (vistaAgregar.jComboBoxMes.getSelectedIndex() == 0)
                || (vistaAgregar.jComboBoxAnyo.getSelectedIndex() == 0)
                || (descripcion.equals(vacio)) || emisor.equals(vacio) || valor.equals(vacio)) {
            JOptionPane.showMessageDialog(vistaAgregar, "Rellene todos los cammpos");
            vistaAgregar.dispose();
            JFrameAgregarIngreso agr = new JFrameAgregarIngreso();
            agr.setLocationRelativeTo(null);
            agr.setVisible(true);
        } else {

            switch ((String) vistaAgregar.jComboBoxTipoPeriodo.getSelectedItem()) {
                case "Seleccione el periodo":
                    JOptionPane.showMessageDialog(vistaAgregar, "Agrege un tipo de periodicidad valido");
                    vistaAgregar.dispose();
                    JFrameAgregarIngreso vistaAgregarCreditos = new JFrameAgregarIngreso();
                    vistaAgregarCreditos.setLocationRelativeTo(null);
                    vistaAgregarCreditos.setVisible(true);
                case "Mensual":
                    modeloIngreso.setTipoPeriodo_id(1);
                    break;
                case "Anual":
                    modeloIngreso.setTipoPeriodo_id(2);
                    break;
                case "Ocasional/Manual":
                    modeloIngreso.setTipoPeriodo_id(3);
                    break;
                case "CREDITO":
                    modeloIngreso.setTipoPeriodo_id(4);
                    break;
                case "Bimestral":
                    modeloIngreso.setTipoPeriodo_id(5);
                    break;
                default:
                    modeloIngreso.setTipoPeriodo_id(6);
                    break;

            }
            modeloIngreso.setDescripcion(descripcion);
            modeloIngreso.setFechaCreacion(fecha);
            modeloIngreso.setEmisor(emisor);
            double ValorADouble = Double.parseDouble(valor);
            modeloIngreso.setValor(ValorADouble);
            this.cuentaSeleccionada_id = JFrameAgregarIngreso.cuentaJF_id[vistaAgregar.jComboBoxCuentaAsociada.getSelectedIndex()];
            modeloIngreso.setCuenta_id(cuentaSeleccionada_id);
        }
        try {
            modeloIngreso.save();
            JPIngresos.addTableValuesIngresos();
            vistaAgregar.dispose();
            System.out.println("La id de la cuenta seleccionada para el ingreso es : " + cuentaSeleccionada_id);
        } catch (DBDriverNotFound ex) {
            Logger.getLogger(ControladorAgregarIngreso.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorAgregarIngreso.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
