package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.ConceptoGastoDAO;
import vistas.JFrameAgregarGasto;
import static vistas.JPPrestamos.addTableValues;

public class ControladorAgregarGasto extends ControladorMovimientos implements ActionListener {

    JFrameAgregarGasto vistaAgregarGasto;
    ConceptoGastoDAO modeloGastos = new ConceptoGastoDAO();
    String descripcion;
    public static int categoriaGasto;
    double valorConsulta;
    public static int consultaID;
    public static int tipoCategoriaGasto_id;


    public ControladorAgregarGasto(JFrameAgregarGasto vistaAgregarGasto) {
        this.vistaAgregarGasto = vistaAgregarGasto;
        this.vistaAgregarGasto.jButtonAgregarGasto.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AgregarGasto();

    }

    public void AgregarGasto() {
        String vacio = "";
        descripcion = vistaAgregarGasto.jTextFieldDescripcion.getText();
        categoriaGasto = vistaAgregarGasto.jComboBoxCategoriaGasto.getSelectedIndex();
        String emisor = vistaAgregarGasto.jTextFieldEmisor.getText();
        String fecha;
        int cuentaRelacionada = vistaAgregarGasto.comboBoxCuentaRelacionada_id[vistaAgregarGasto.jComboBoxCuentaRelacionada.getSelectedIndex()];
        System.out.println("El ID de la cuenta relacionada es" + cuentaRelacionada);
        int tipoPeriodo = vistaAgregarGasto.jComboBoxTipoPeriodo.getSelectedIndex();
        String valor = vistaAgregarGasto.jTextFieldImporte.getText();
        Double valorDouble = Double.parseDouble(valor);
        if (descripcion.equals(vacio) || categoriaGasto==0 || emisor.equals(vacio)
                || (vistaAgregarGasto.jComboBoxDia.getSelectedIndex() == 0)
                || (vistaAgregarGasto.jComboBoxMes.getSelectedIndex() == 0)
                || (vistaAgregarGasto.jComboBoxAnyo.getSelectedIndex() == 0)
                || cuentaRelacionada == 0 || tipoPeriodo == 0) {

            vistaAgregarGasto.dispose();
            JOptionPane.showMessageDialog(vistaAgregarGasto, "Por favor, rellene todos los cammpos");
            JFrameAgregarGasto gasto = new JFrameAgregarGasto();
            gasto.setLocationRelativeTo(null);
            gasto.setVisible(true);
        } else {
            fecha = vistaAgregarGasto.jComboBoxAnyo.getSelectedItem() + "-"
                    + vistaAgregarGasto.jComboBoxMes.getSelectedItem() + "-"
                    + vistaAgregarGasto.jComboBoxDia.getSelectedItem();
            modeloGastos.setDescripcion(descripcion);
            //modeloGastos.setTipoCategoriaGasto_id(categoriaGasto);
            modeloGastos.setEmisor(emisor);
            modeloGastos.setFechaCreacion(fecha);
            //modeloGastos.setFechaBaja(fecha);
            modeloGastos.setCuenta_id(cuentaRelacionada);
            modeloGastos.setTipoPeriodo_id(tipoPeriodo);
            modeloGastos.setValor(valorDouble);
            valorConsulta = valorDouble;
            modeloGastos.setEsExacto((byte) 1);

            switch (categoriaGasto) {

                

                case 1:
                    modeloGastos.setTipoCategoriaGasto_id(1);
                    tipoCategoriaGasto_id = 1;
                    break;
                case 2:
                    modeloGastos.setTipoCategoriaGasto_id(2);
                    tipoCategoriaGasto_id = 2;
                    break;
                case 3:
                    modeloGastos.setTipoCategoriaGasto_id(3);
                    tipoCategoriaGasto_id = 3;
                    break;

                case 4:
                    modeloGastos.setTipoCategoriaGasto_id(4);
                    tipoCategoriaGasto_id = 4;
                    break;
                 case 6:
                    modeloGastos.setTipoCategoriaGasto_id(6);
                    tipoCategoriaGasto_id = 6;
                    break;

                 case 5:
                    modeloGastos.setTipoCategoriaGasto_id(5);
                    tipoCategoriaGasto_id = 5;
                    break;

            }

            try {

                modeloGastos.save();
                addTableValues();
                getGastoID();
                System.out.println("gasto ID: "+consultaID);
                System.out.println("cuenta ID:"+cuentaRelacionada);
                //getCategoriaGastoID();
                
                MySQLBD mysql = new MySQLBD().conectar();
                mysql.ejecutar("INSERT INTO Movimientos(cuenta_id, Descripcion, Fecha, Valor, Tipo,"
                        + " ConceptoGasto_id) "
                        + "VALUES ("+cuentaRelacionada+", '" + descripcion + "', '" + fecha + "'," + valorDouble + "," + "'S',"
                        + consultaID+");");
                this.vistaAgregarGasto.dispose();
            } catch (DBDriverNotFound ex) {
                Logger.getLogger(ControladorAgregarGasto.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorAgregarGasto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getGastoID() throws SQLException {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("select conceptogasto_id from conceptosgastos inner join "
                + "usuarios_cuentas ON usuarios_cuentas.usuario_id=" + ControladorIniciarSesion.ID
                + " && usuarios_cuentas.cuenta_id=conceptosgastos.cuenta_id && conceptosgastos.descripcion= '" + descripcion + "' "
                + "&& conceptosgastos.valor = " + valorConsulta + ";");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("conceptoGasto_id"));

            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.consultaID = resultadoID.getInt("conceptoGasto_id");
        return consultaID;
    }

    
}
