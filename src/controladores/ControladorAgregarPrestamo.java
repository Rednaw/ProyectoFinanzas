package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.PrestamoDAO;
import vistas.JFrameAgregarPrestamo;
import vistas.JPPrestamos;

public class ControladorAgregarPrestamo implements ActionListener {

    JFrameAgregarPrestamo vistaAgregarPrestamo;
    PrestamoDAO modeloPrestamos = new PrestamoDAO();
    public static int prestamo_id;

    public ControladorAgregarPrestamo(JFrameAgregarPrestamo vistaAgregarPrestamo) {
        this.vistaAgregarPrestamo = vistaAgregarPrestamo;
        this.vistaAgregarPrestamo.jButtonAgregar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String vacio = "";
        String descripcion = vistaAgregarPrestamo.jTextFieldDescripcion.getText();
        String tae = vistaAgregarPrestamo.jTextFieldTipoTAE.getText();
        String limite = vistaAgregarPrestamo.jTextField_Limite_Saldo.getText();
        String fecha;
        Double taeADouble = Double.parseDouble(tae);
        Double limiteADouble = Double.parseDouble(limite);
        int cuentaSelected = vistaAgregarPrestamo.jComboBoxCuenta.getSelectedIndex();
        int cuentaIdValue = vistaAgregarPrestamo.comboBoxCuenta_id[cuentaSelected];
        int tipoPeriodoSelected = vistaAgregarPrestamo.jComboBoxTipoPeriodo.getSelectedIndex();
        if ((vistaAgregarPrestamo.jComboBoxDia.getSelectedIndex() == 0)
                || (vistaAgregarPrestamo.jComboBoxMes.getSelectedIndex() == 0)
                || (vistaAgregarPrestamo.jComboBoxAnyo.getSelectedIndex() == 0)
                || (descripcion.equals(vacio)) || tae.equals(vacio) || limite.equals(vacio)
                || (taeADouble == 0.0) || limiteADouble == 0.0) {

            vistaAgregarPrestamo.dispose();
            JOptionPane.showMessageDialog(vistaAgregarPrestamo, "Rellene todos los cammpos");
            JFrameAgregarPrestamo pre = new JFrameAgregarPrestamo();
            pre.setLocationRelativeTo(null);
            pre.setVisible(true);

        } else {

            fecha = vistaAgregarPrestamo.jComboBoxAnyo.getSelectedItem() + "-"
                    + vistaAgregarPrestamo.jComboBoxMes.getSelectedItem() + "-"
                    + vistaAgregarPrestamo.jComboBoxDia.getSelectedItem();
            modeloPrestamos.setFechaCreacion(fecha);
            modeloPrestamos.setDescripcion(descripcion);
            modeloPrestamos.setCuenta_id(cuentaIdValue);
            modeloPrestamos.setLimite(limiteADouble);
            modeloPrestamos.setTipoPeriodo_id(tipoPeriodoSelected);
            modeloPrestamos.setNumCuotas(1);
            modeloPrestamos.setTipoTAE(taeADouble);
            modeloPrestamos.setLimite(limiteADouble);
            modeloPrestamos.setSaldo(limiteADouble);
            modeloPrestamos.setNumCuotas(1);
            try {
                modeloPrestamos.save();

                vistaAgregarPrestamo.dispose();
                JOptionPane.showMessageDialog(vistaAgregarPrestamo, "Prestamo Agregado");
                
                JPPrestamos.addTableValues();
                
                
            } catch (DBDriverNotFound ex) {
                Logger.getLogger(ControladorAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
