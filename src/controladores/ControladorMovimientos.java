package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.MovimientoDAO;

public class ControladorMovimientos {

    public ControladorMovimientos() {

    }

    public void InsertMovimientos( int cuenta_id, String descripcion, String fecha,
            double valor, char tipo, int tarjeta_id, int credito_id, int prestamo_id,
            int conceptoIngreso_id, int conceptoGasto_id) {
        MovimientoDAO modeloMovimiento = new MovimientoDAO();
        
        if(cuenta_id!=0){
            modeloMovimiento.setCuenta_id(cuenta_id);
            
        }

        /*if (tarjeta_id != 0) {
            modeloMovimiento.setTarjeta_id(tarjeta_id);
        }*/
        if (credito_id != 0) {
            modeloMovimiento.setCredito_id(credito_id);
        }
        if (prestamo_id != 0) {
            modeloMovimiento.setPrestamo_id(prestamo_id);

        }
        if (conceptoIngreso_id != 0) {
            modeloMovimiento.setConceptoIngreso_id(conceptoIngreso_id);
        }
        if (conceptoIngreso_id != 0) {
            modeloMovimiento.setConceptoIngreso_id(conceptoIngreso_id);
            
        }
        if (conceptoGasto_id != 0) {
            modeloMovimiento.setConceptoGasto_id(conceptoGasto_id);
        }
        //modeloMovimiento.setCuenta_id(cuenta_id);
        //modeloMovimiento.setConceptoGasto_id(conceptoGasto_id);
        modeloMovimiento.setDescripcion(descripcion);
        modeloMovimiento.setFecha(fecha);
        modeloMovimiento.setValor(valor);
        modeloMovimiento.setTipo(tipo);
        // modeloMovimiento.setTarjeta_id(tarjeta_id);
        //modeloMovimiento.setCredito_id(credito_id);
        //modeloMovimiento.setPrestamo_id(prestamo_id);
        //modeloMovimiento.setConceptoIngreso_id(conceptoIngreso_id);
        //modeloMovimiento.setConceptoGasto_id(conceptoGasto_id);
        try {
            modeloMovimiento.save();
        } catch (DBDriverNotFound ex) {
            Logger.getLogger(ControladorMovimientos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorMovimientos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
