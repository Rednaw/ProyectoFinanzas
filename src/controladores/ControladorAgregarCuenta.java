package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import static controladores.ControladorUsuarioNuevo.usuarioNuevo_id;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.CuentaDAO;
import vistas.JFFame;
import vistas.JFrameAgregarCuenta;
import vistas.JFrameBienvenidos;
import vistas.JPCuentas;

public class ControladorAgregarCuenta implements ActionListener {

    public static int cuenta_id;

    JFrameAgregarCuenta vistaAgregarCuenta = new JFrameAgregarCuenta();
    CuentaDAO modeloCuenta = new CuentaDAO();

    public ControladorAgregarCuenta(JFrameAgregarCuenta vistaAgregarCuenta) {
        this.vistaAgregarCuenta = vistaAgregarCuenta;
        this.vistaAgregarCuenta.jButtonAgregar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int cAhorro = 1;
        int cCorriente = 2;
        String vacio = "";
        String numeroIBAN = vistaAgregarCuenta.jTextFieldNumeroIBAN.getText();
        String descripcion = vistaAgregarCuenta.jTextAreaDescripcion.getText();
        String dia = vistaAgregarCuenta.jComboBoxDia.getSelectedItem().toString();
        String mes = vistaAgregarCuenta.jComboBoxMes.getSelectedItem().toString();
        String anyo = vistaAgregarCuenta.jComboBoxAnyo.getSelectedItem().toString();
        String tipoCuenta = vistaAgregarCuenta.jComboBoxTipoCuenta.getSelectedItem().toString();
        String entidad = vistaAgregarCuenta.jTextFieldEntidad.getText();
        String fecha = anyo + "-" + mes + "-" + dia;
        System.out.println(fecha);
        if (numeroIBAN.equals(vacio) || mes.equals("") || dia.equals("") || anyo.equals("")
                || tipoCuenta.equals(vacio) || anyo.equals("AÑO") || dia.equals("DIA") || mes.equals("MES")) {
            JOptionPane.showMessageDialog(vistaAgregarCuenta, "Formato invalido, introduzca bien los campos");
            vistaAgregarCuenta.dispose();
            JFrameAgregarCuenta vista = new JFrameAgregarCuenta();
            vista.setLocationRelativeTo(null);
            vista.setVisible(true);

        } else {
            modeloCuenta.setDescripcion(descripcion);
            modeloCuenta.setFechaCreacion(fecha);
            modeloCuenta.setEntidad(entidad);
            modeloCuenta.setNumeroIban(numeroIBAN);
            if (tipoCuenta.equals("Ahorros")) {
                modeloCuenta.setTipoCuenta(cAhorro);
            } else {
                modeloCuenta.setTipoCuenta(cCorriente);
            }

            try {

                modeloCuenta.save();
                getCuentaID();
                if (ControladorIniciarSesion.ID == 0) {
                    MySQLBD mysql = new MySQLBD().conectar();
                    mysql.ejecutar("INSERT INTO usuarios_cuentas(usuario_id, cuenta_id) VALUES"
                            + " (" + usuarioNuevo_id + ", " + cuenta_id + ")" + ";");
                } else {
                    MySQLBD mysql = new MySQLBD().conectar();
                    mysql.ejecutar("INSERT INTO usuarios_cuentas(usuario_id, cuenta_id) VALUES"
                            + " (" + ControladorIniciarSesion.ID + ", " + cuenta_id + ")" + ";");

                }
                System.out.println("nosotros" + cuenta_id);
                this.vistaAgregarCuenta.dispose();
                if (ControladorIniciarSesion.ID != 0) {
                    JPCuentas.jListCuentasBancarias();
                }else{
                    JFrameBienvenidos v=new JFrameBienvenidos();
                    v.setLocationRelativeTo(null);
                    v.setVisible(true);
                }
            } catch (DBDriverNotFound ex) {
                Logger.getLogger(ControladorAgregarCuenta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorAgregarCuenta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public int getCuentaID() throws SQLException {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT cuenta_id from cuentas WHERE Descripcion = '"
                + modeloCuenta.getDescripcion() + "' && NumeroIBAN = '" + modeloCuenta.getNumeroIban() + "';");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("cuenta_id"));
            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.cuenta_id = resultadoID.getInt("cuenta_id");
        return cuenta_id;
    }

}
